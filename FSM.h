/*
 * FSM.h
 *
 *  Created on: Apr 13, 2019
 *      Author: Vlad A.
 */

#ifndef FSM_H_
#define FSM_H_

#include <type_traits>
#include <stddef.h>

typedef void (*StateFunction)();

/***********************************************
 *
 *  Container template definition
 *
 ***********************************************/

template< typename...T > struct Container
{
    constexpr void set() {}
};

template< typename First, typename ...Rest >
struct Container< First, Rest...> : Container<Rest...>
{
    First first;
    Container<Rest...> rest;
    constexpr Container() : first(First(0)), rest() {}
    constexpr void set(const First f,  Rest...r )
    {
        first = f;
        rest.set(r...);
    }
};

template < size_t, class > struct TypeHolder;

template < class T, class...Ts>
struct TypeHolder< 0, Container< T,Ts... > >
{
    typedef T type;
};

template < size_t idx, class T, class...Ts>
struct TypeHolder< idx, Container< T, Ts... > >
{
    typedef typename TypeHolder< idx-1, Container< Ts... > >::type type;
};

template <size_t idx, class... Ts>
typename std::enable_if< idx == 0, typename TypeHolder< 0, Container<Ts...>>::type>::type
get(Container<Ts...> t)
{
    return t.first;
}

template <size_t idx, class T, class... Ts>
typename std::enable_if< idx != 0, typename TypeHolder< idx, Container<T, Ts...>>::type>::type
get(Container<T, Ts...> t)
{
    Container<Ts...> base = t.rest;
    return get<idx - 1>(base);
}

/**********************************
 *                                *
 *    *****   *****  **     **    *
 *    **     **      ***   ***    *
 *    ****    ****   **** ****    *
 *    **         **  ** *** **    *
 *    **     *****   **  *  **    *
 *                                *
 **********************************/

template< typename Trigger_t, typename State_t, int stateCount, typename ... stateConsts >
class FSM
{
private:
    struct StateParams_t
    {
        int           timeout;
        Trigger_t     Condition[ stateCount ];
        StateFunction Once;
        StateFunction Always;
        Container<stateConsts...> Params;

        constexpr StateParams_t( int TO = -1 )
            : timeout(TO)
            , Condition{}
            , Once(0)
            , Always(0)
            {}
    };

    struct States_t
    {
        StateParams_t state[ stateCount ];
        constexpr States_t() : state{} {}
    };

    static const States_t Machine;

public:
    class StateBuilder
    {
        StateParams_t intA;
    public:
        constexpr StateBuilder( int TO = -1 ) : intA(TO) {}
        constexpr StateParams_t  buildState() const { return intA; }

        constexpr StateBuilder   setConsts( stateConsts... args )
        { return (this->intA.Params.set(args...), *this); }
        constexpr StateBuilder   switchCondition( Trigger_t x, State_t i )
        { return (this->intA.Condition[ int(i) ] = x, *this); }
        constexpr StateBuilder   callOnce(   StateFunction fn)
        { return (this->intA.Once   = fn, *this); }
        constexpr StateBuilder   callAlways( StateFunction fn)
        { return (this->intA.Always = fn, *this); }
    };

    class MachineBuilder
    {
        States_t intB;
    public:
        constexpr States_t       buildMachine() const { return intB; }
        constexpr MachineBuilder defineState( State_t i, StateParams_t x)
        { return (this->intB.state[int(i)] = x, *this); }
    };

public:
    State_t state;
    int     stateTimeout;

    FSM() : state( State_t(0) ), stateTimeout( Machine.state[0].timeout ) {}
    virtual ~FSM() {}

    virtual void __init__() { state = State_t(0); stateTimeout = Machine.state[0].timeout; }
    virtual void __tick__()
    {

        if ( stateTimeout == Machine.state[ int(state) ].timeout && Machine.state[ int(state) ].Once )
            Machine.state[ int(state) ].Once();
        else
        {  if ( Machine.state[ int(state) ].Always ) Machine.state[ int(state) ].Always(); }

        if ( stateTimeout > 0 )
            stateTimeout--;

        for( int s = 0; s < stateCount; s++ )
            if ( checkTrigger( SwitchCondition( state, State_t( s ) ) ) )
            {
                newState( State_t( s ) );
                break;
            }
    }

protected:
    virtual void newState( const State_t S )
    {
        state        = S;
        stateTimeout = Machine.state[ int(S) ].timeout;
    }

    virtual bool checkTrigger( const Trigger_t T )
    {
        switch( int( T ) )
        {
            case 1: return ( stateTimeout == 0 );  // Timeout
            case 2: return true;                   // Always
        }
        return false;
    }

    inline  Trigger_t SwitchCondition( const State_t from, const State_t to )
    { return Machine.state[ int( from ) ].Condition[ int( to ) ]; }
    template<size_t idx>
    inline auto getConst()
    { return get<idx>(Machine.state[ int( state ) ].Params); }
};

#define CLASSNAME FSM<Trigger_t, State_t, stateCount, stateConsts... >

template< typename Trigger_t, typename State_t, int stateCount, typename ... stateConsts >
const typename CLASSNAME::States_t CLASSNAME::Machine  = CLASSNAME::MachineBuilder().buildMachine();

#endif /* FSM_H_ */
