/*
 * FSM_Build.h
 *
 *  Created on: Apr 16, 2019
 *      Author: Vlad A.
 */

#ifndef MAIN_FSM_BUILD_H_
#define MAIN_FSM_BUILD_H_

/*
 * Example of FSM Definition.
 *

#include "FSM.h"

using BaseFSM = FSM< Trigger, States, int(States::MAX), Lock_t, Servo_t, Stepper_t > ;
using State_b = BaseFSM::StateBuilder;
using FSM_b   = BaseFSM::MachineBuilder;

constexpr State_b stRandom        = State_b( 3 )   .setConsts( 0,  1,  0 )
    .switchCondition( Trigger::Timeout,       States::SearchinHome );

constexpr State_b stSearchinHome  = State_b( 10 )  .setConsts( 0,  0,  1)
    .switchCondition( Trigger::TargetReached, States::CheckingHome  )
    .switchCondition( Trigger::Timeout,       States::Error );

constexpr State_b stCheckingHome  = State_b( 5 )   .setConsts( 0,  0,  1 )
    .switchCondition( Trigger::Always,        States::Hold );

constexpr State_b stIdle          = State_b( 10 )  .setConsts( 1,  0,  0 )
    .switchCondition( Trigger::Wakeup,        States::LockedToHold );

constexpr State_b stIdleLocked    = State_b( 3 )   .setConsts( 1,  1,  0 )
    .switchCondition( Trigger::Timeout,       States::Idle )
    .switchCondition( Trigger::Wakeup,        States::LockedToHold );

constexpr State_b stLockedToHold  = State_b( 3 )   .setConsts( 0,  1,  0 )
    .switchCondition( Trigger::Timeout,       States::Hold );

constexpr State_b stHoldToLocked  = State_b( 3 )   .setConsts( 1,  1,  1 )
    .switchCondition( Trigger::Timeout,       States::IdleLocked )
    .switchCondition( Trigger::Wakeup,        States::LockedToHold );

constexpr State_b stHold          = State_b( 25 )  .setConsts( 0,  0,  1)
    .switchCondition( Trigger::Timeout,       States::HoldToLocked )
    .switchCondition( Trigger::Wakeup,        States::Moving );

constexpr State_b stMoving        = State_b( 500 ) .setConsts( 0,  0,  1 )
    .switchCondition( Trigger::Timeout,       States::Moving )
    .switchCondition( Trigger::TargetReached, States::Hold );

constexpr FSM_b myBuilder = FSM_b()
    .defineState( States::Random,       stRandom      .buildState() )
    .defineState( States::SearchinHome, stSearchinHome.buildState() )
    .defineState( States::CheckingHome, stCheckingHome.buildState() )
    .defineState( States::Idle,         stIdle        .buildState() )
    .defineState( States::IdleLocked,   stIdleLocked  .buildState() )
    .defineState( States::LockedToHold, stLockedToHold.buildState() )
    .defineState( States::HoldToLocked, stHoldToLocked.buildState() )
    .defineState( States::Hold,         stHold        .buildState() )
    .defineState( States::Moving,       stMoving      .buildState() );

template<>
const typename BaseFSM::States_t BaseFSM::Machine  = myBuilder.buildMachine();


 */
#define declFSMintConst( Base, Builder ) template <> const typename Base::States_t Base::Machine = Builder.buildMachine()

#endif /* MAIN_FSM_BUILD_H_ */
